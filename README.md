# Development Environment

Use `docker` to set up a local development environment. Ensure you have both `docker` and
`docker-compose` installed on your system, then run:

```
docker-compose up
```

Wordpress should be up at `localhost:8080`.

The first time you do this, you will need to go through the Wordpress installation process.

Use the following:

- Language: English (United States)
- Site title: TEDxWarsaw

The rest is up to you.

Once installed, log in to Admin and go to Appearance -> Themes. Select `tedxwarsaw`. You may also
want to go to Admin -> Users -> Profile and uncheck `Show Toolbar when viewing site`. Then go to
Appearance -> Menus and create a new menu called Primary with any pages you want. Then go to the
Manage Locations tab on the same page and for the theme location `Primary` choose the menu `Primary`.
Add a menu called `Footer` and in Manage Locations choose the `Footer` location.
You can set a home page by going to Appearance -> Customize -> Homepage Settings.
